module github.com/mdouchement/seikan

go 1.18

require (
	github.com/flynn/noise v1.0.0
	github.com/fxamacker/cbor/v2 v2.4.0
	github.com/hashicorp/go-multierror v1.1.1
	github.com/hashicorp/yamux v0.0.0-20211028200310-0bc27b27de87
	github.com/klauspost/compress v1.15.7
	github.com/mdouchement/basex v0.0.0-20200802103314-a4f42a0e6590
	github.com/mdouchement/logger v0.0.0-20200719134033-63d0eda5567d
	github.com/mendsley/gomnet v0.0.0-20150905011337-ac10ddc4555c
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.5.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
	golang.org/x/term v0.0.0-20220526004731-065cf7ba2467 // indirect
)
